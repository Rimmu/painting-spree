import { PaintingSpreePage } from './app.po';

describe('painting-spree App', () => {
  let page: PaintingSpreePage;

  beforeEach(() => {
    page = new PaintingSpreePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
