import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPaintComponent } from './add-paint.component';

describe('AddPaintComponent', () => {
  let component: AddPaintComponent;
  let fixture: ComponentFixture<AddPaintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPaintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPaintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
