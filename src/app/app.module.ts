import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes} from '@angular/router';

//MyCompos
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { PaintsComponent } from './components/paints/paints.component';
import { PaintComponent } from './components/paint/paint.component';
import { AddPaintComponent } from './components/add-paint/add-paint.component';
import { EditPaintComponent } from './components/edit-paint/edit-paint.component';
import { FooterComponent } from './components/footer/footer.component';

const appRoutes = [
  {path: '', component: HomeComponent},
  {path: 'paints', component: PaintsComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    PaintsComponent,
    PaintComponent,
    AddPaintComponent,
    EditPaintComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
